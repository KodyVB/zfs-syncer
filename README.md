# Synopsis

Zfs-syncer is a program that will scan through the snapshots of two given zfs subvolume (named SOURCE and SINK), and then run `zfs destroy` on any snapshots that are in the SINK that aren't in the SOURCE.
It works with both local file pools as well as through SSH.

The reason behind making this was because [Sanoid/Syncoid](https://github.com/jimsalterjrs/sanoid/)
 automates the snapshot creation, pruning, and syncing processes between pools, but does not prune snapshots that are in the target subvolume.
 Zfs-syncer has been created so it could be added to Sanoid/Syncoid cron jobs to keep the target directory clean like the source directory in cases where installing Sanoid on the target machine is impractical or impossible.
But the program can be used independently of Sanoid, too.

# Installation

Zfs-syncer is a single shell script, so installing it is as easy as copying or linking it to a directory in one's PATH variable, e.g. `/usr/local/bin/`, `/usr/bin/`, or `/bin/`.

# Usage

It's recommended to use the `-nv` flags before running the program to make sure the desired snapshots will be deleted.

Usage: `zfs-syncer [OPTIONS] SOURCE SINK`

Mandatory arguments are:

* `SOURCE`: The zfs subvolume that the SINK will try to match
* `SINK`: The zfs subvolume that will delete extra snapshots of to try to match the SOURCE

Optional arguments are:

* `-h`: Presents this help information
* `-I [KEY FILE]`: Absolute path to the SOURCE's ID file, if needed
* `-i [KEY FILE]`: Absolute path to the SINK's ID file, if needed
* `-n`: Dry-run - runs 'zfs destroy' with the '-n' flag
* `-P [SSH PORT]`: The SOURCE's SSH port, if not using standard port (22)
* `-p [SSH PORT]`: The SINK's SSH port, if not using standard port (22)
* `-R`: Makes the 'zfs list' command recursive with '-r' for SOURCE.
* `-r`: Makes the 'zfs list' command recursive with '-r' for SINK.
* `-v`: Verbose - runs 'zfs destroy' with the '-v' flag

In order to use SSH, zfs-syncer looks for the following format: `user@domain:zpool/zfs/path`.
Otherwise, it expects the following format for local pools: `zpool/zfs/path`.

# Notes

This only compares the snapshots' names, for example `zroot/data/home@autosnap_2021-06-11_21:00:00_hourly` would only be comparing `autosnap_2021-06-11_21:00:00_hourly`.
This is because the parent of the snapshot, i.e. 'home', could differ between the SOURCE and SINK.

If getting an error like `cannot open '<pool>': missing '@' delimiter in snapshot name` it may be that the '-r' or '-R' flag is required.

